﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UnitTests
{
    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class Tests
    {
        IApp app;
        Platform platform;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void Setup()
        {
            app = ConfigureApp.iOS.AppBundle("/path/to/iosapp.app").StartApp();
        }

        [Test]
        public void AppLaunches()
        {
            app.Tap(c => c.Marked("MyStuff.app"));
        }
    }
}
